<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    // Create new instance of task_comment
    $task_comment = new Task_Comment($db);

    //Get USER input
    $data = json_decode(file_get_contents('php://input'));

    // Assign user input to task_comment object
    $task_comment->id = $data->id;
    $task_comment->body = $data->body;

    // Update task_comment
    if($task_comment->update()){
        echo json_encode(array(
            "message" => "task_comment Updated"
        ));
    } else {
        son_encode(array(
            "message" => "Update Failed"
        ));
    }
?>
