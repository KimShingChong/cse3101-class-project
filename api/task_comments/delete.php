<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: Delete');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog task_comment object
    $task_comment = new Task_Comment($db);

    // Get $_task_comment Data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to be deleted
    $task_comment->id = $data->id;

    // Update the task_comment
    if($task_comment->delete()){
        echo json_encode(array("message" => "Comment Deleted"));
    } else {
        echo json_encode(array("message" => "Not Deleted"));

    }
?>