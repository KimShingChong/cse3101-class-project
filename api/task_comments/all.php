<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog task_comment object
    $task_comment = new Task_Comment($db);

    //task_comment query
    $result = $task_comment->read();

    //get row count
    $count = $result->rowCount();

    // Check if task_comments exist
    if($count > 0){
        // Create array if exist
        $task_comment_arr = array();
        $task_comment_arr['data'] = array();
      //  $task_comment_arr['test'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
           // $task_comments = array('message' => "meh");
            $task_comment_item = array(
                'id' => $id,
                'author' => $author,
                'body' => $body,
                'project' => $project,
                'created_at' => $created_at
            );

        array_push($task_comment_arr['data'], $task_comment_item);
       // array_push($task_comment_arr['test'], $task_comments);
        }
        
        echo json_encode($task_comment_arr);
        // echo json_encode($task_comment_arr);
    } else {
        echo json_encode(array('message' => 'No Comments avaialble'));
    }
?>