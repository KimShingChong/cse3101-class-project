<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $task_comment = new Task_Comment($db);
 
    // Get post data
    $data = json_decode(file_get_contents("php://input"));
    
    // Assign data
    $task_comment->author = $data->author;
    $task_comment->body = $data->body;
    $task_comment->task_id = $data->task_id;

    // Create task_comment
    if($task_comment->create()){
        echo 'Created';
    } else {
        echo 'failed'; 
    }
?>