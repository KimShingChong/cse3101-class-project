<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Project_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    // Create new instance of project_comment
    $project_comment = new Project_Comment($db);

    //Get USER input
    $data = json_decode(file_get_contents('php://input'));

    // Assign user input to project_comment object
    $project_comment->id = $data->id;
    $project_comment->body = $data->body;

    // Update project_comment
    if($project_comment->update()){
        echo json_encode(array(
            "message" => "project_comment Updated"
        ));
    } else {
        son_encode(array(
            "message" => "Update Failed"
        ));
    }
?>
