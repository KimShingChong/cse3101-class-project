<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Project_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $project_comment = new Project_Comment($db);
 
    // Get post data
    $data = json_decode(file_get_contents("php://input"));

    // Assign data
    $project_comment->author = $data->author;
    $project_comment->body = $data->body;
    $project_comment->task_id = $data->task_id;

    // Create project_comment
    if($project_comment->create()){
        echo 'Created';
    } else {
        echo 'failed'; 
    }
?>