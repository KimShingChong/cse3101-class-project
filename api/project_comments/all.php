<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Project_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog project_comment object
    $project_comment = new Project_Comment($db);

    //project_comment query
    $result = $project_comment->read();

    //get row count
    $count = $result->rowCount();

    // Check if project_comments exist
    if($count > 0){
        // Create array if exist
        $project_comment_arr = array();
        $project_comment_arr['data'] = array();
      //  $project_comment_arr['test'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
           // $project_comments = array('message' => "meh");
            $project_comment_item = array(
                'id' => $id,
                'author' => $author,
                'body' => $body,
                'project' => $project,
                'created_at' => $created_at
            );

        array_push($project_comment_arr['data'], $project_comment_item);
       // array_push($project_comment_arr['test'], $project_comments);
        }
        
        echo json_encode($project_comment_arr);
        // echo json_encode($project_comment_arr);
    } else {
        echo json_encode(array('message' => 'No Comments avaialble'));
    }
?>