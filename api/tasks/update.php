<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Task.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    // Create new instance of task
    $task = new Task($db);

    //Get USER input
    $data = json_decode(file_get_contents('php://input'));

    // Assign user input to task object
    $task->id = $data->id;
    $task->title = $data->title;
    $task->project = $data->project;
    $task->assigned_to = $data->assigned_to;
    $task->start_date = $data->start_date;
    $task->end_date = $data->end_date;
    $task->created_by = $data->created_by;

    // Update task
    if($task->update()){
        echo json_encode(array(
            "message" => "Task Updated"
        ));
    } else {
        son_encode(array(
            "message" => "Update Failed"
        ));
    }
?>