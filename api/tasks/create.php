<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Task.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $task = new Task($db);
 
    // Get post data
    $data = json_decode(file_get_contents("php://input"));

    // Assign data
    $task->id = $data->id;
    $task->title = $data->title;
    $task->project = $data->project;
    $task->assigned_to = $data->assigned_to;
    $task->start_date = $data->start_date;
    $task->end_date = $data->end_date;
    $task->created_by = $data->created_by;

    // Create task
    if($task->create()){
        echo 'Created';
    } else {
        echo 'failed'; 
    }
?>