<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Task.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog task object
    $task = new Task($db);
    $comments = new Task_Comment($db);

    //task query
    $result = $task->read();

    //get row count
    $count = $result->rowCount();

    // Check if tasks exist
    if($count > 0){
        // Create array if exist
        $task_arr = array();
        $task_arr['data'] = array();
      //  $task_arr['test'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
           // $tasks = array('message' => "meh");
            $task_item = array(
                'id' => $id,
                'title' => $title,
                'project' => $project,
                'assigned_to' => $assigned_to,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'created_by' => $created_by,
                'comments' =>   $comments->read_single($id)
            );

        array_push($task_arr['data'], $task_item);
       // array_push($task_arr['test'], $tasks);
        }
        
        echo json_encode($task_arr);
        // echo json_encode($task_arr);
    } else {
        echo json_encode(array('message' => 'No tasks avaialble'));
    }
?>