<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Task.php';
    include_once '../../models/Task_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog task object
    $task = new Task($db);
    $comments = new Task_Comment($db);

    // Get id
    $task->id = isset($_GET['id']) ? $_GET['id'] : die();

    $task->read_single();

    $task_arr = array(
        'id' => $task->id,
        'title' => $task->title,
        'project' => $task->project,
        'assigned_to' => $task->assigned_to,
        'start_date' => $task->start_date,
        'end_date' => $task->end_date,
        'created_by' => $task->created_by,
        'created_at' => $task->created_at,
        'comments' => $comments->read_single($task->id)
    );

    echo json_encode($task_arr);
?>