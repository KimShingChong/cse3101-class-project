<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Project.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $project = new Project($db);
 
    // Get post data
    $data = json_decode(file_get_contents("php://input"));

    // Assign data
    $project->title = $data->title;
    $project->start_date = $data->start_date;
    $project->due_date = $data->due_date;
    $project->created_by = $data->created_by;

    // Create project
    if($project->create()){
        echo 'Created';
    } else {
        echo 'failed'; 
    }
?>