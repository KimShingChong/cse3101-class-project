<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Project.php';
    include_once '../../models/Project_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog project object
    $project = new Project($db);
    $comments = new Project_Comment($db);

    // Get id
    $project->id = isset($_GET['id']) ? $_GET['id'] : die();

    $project->read_single();

    $project_arr = array(
        'id' => $project->id,
        'title' => $project->title,
        'start_date' => $project->start_date,
        'due_date' => $project->due_date,
        'created_by' => $project->created_by,
        'created_at' => $project->created_at,
        'comments' => $comments->read_single($project->id)
    );

    echo json_encode($project_arr);
?>