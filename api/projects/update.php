<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Project.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    // Create new instance of project
    $project = new Project($db);

    //Get USER input
    $data = json_decode(file_get_contents('php://input'));

    // Assign user input to project object
    $project->id = $data->id;
    $project->title = $data->title;
    $project->start_date = $data->start_date;
    $project->due_date = $data->due_date;

    // Update project
    if($project->update()){
        echo json_encode(array(
            "message" => "Project Updated"
        ));
    } else {
        son_encode(array(
            "message" => "Update Failed"
        ));
    }
?>