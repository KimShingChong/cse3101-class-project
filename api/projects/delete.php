<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: Delete');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/Project.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog project object
    $project = new Project($db);

    // Get $_project Data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to be deleted
    $project->id = $data->id;

    // Update the project
    if($project->delete()){
        echo json_encode(array("message" => "Project Deleted"));
    } else {
        http_response_code(404);
        echo json_encode(array("message" => "Project Not Deleted"));

    }
?>