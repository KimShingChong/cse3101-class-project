<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Project.php';
    include_once '../../models/Project_Comment.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog project object
    $project = new Project($db);
    $comments = new Project_Comment($db);

    //project query
    $result = $project->read();

    //get row count
    $count = $result->rowCount();

    // Check if projects exist
    if($count > 0){
        // Create array if exist
        $project_arr = array();
        $project_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $project_item = array(
                'id' => $id,
                'title' => $title,
                'start_date' => $start_date,
                'due_date' => $due_date,
                'created_by' => $created_by,
                'created_at' => $created_at,
                'comments' => $comments->read_single($id)
            );

        array_push($project_arr['data'], $project_item);
        }
        
        echo json_encode($project_arr);
        // echo json_encode($project_arr);
    } else {
        echo json_encode(array('message' => 'No projects avaialble'));
    }
?>