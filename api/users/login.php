<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/database.php';
    include_once '../../models/User.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $user = new User($db);

    // Get post data
    $data = json_decode(file_get_contents("php://input"));

    // Assign login data
    $user->email = $data->email;
    $user->password = $data->password;

    // Get User if Exists
    $result = $user->login();

    // Return User data if logn successful
    if($result->rowCount()){
        $user_data = array();
        $user_data['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $user = array(
                'id' => $id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
            );
            array_push($user_data['data'], $user);
        }
        echo json_encode($user_data);
    } else {
        $error = array('message' => 'No Account Found With These Credentials');
        http_response_code(404);
        echo json_encode($error);

    }
    

    

?>