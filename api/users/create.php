<?php
    
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/database.php';
    include_once '../../models/User.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate User object
    $user = new User($db);

    // Get post data
    $data = json_decode(file_get_contents("php://input"));

    // Assign data
    $user->email = $data->email;
    $user->password = $data->password;
    $user->first_name = $data->first_name;
    $user->last_name = $data->last_name;
    $user->account_type = $data->account_type;

    // Create User
    if($user->create()){
        echo json_encode(array("message" => "User Created Successfully"));
    } else {
        echo json_encode(array("message" => "Unable to create user"));
}
?>