<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/User.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog user object
    $user = new User($db);

    //User query
    $result = $user->get_all();

    //get row count
    $count = $result->rowCount();

    // Check if users exist
    if($count > 0){
        // Create array if exist
        $user_arr = array();
        $user_arr['data'] = array();
      //  $user_arr['test'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
           // $users = array('message' => "meh");
            $user_item = array(
                'id' => $id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'account_type' => $account_type
            );

        array_push($user_arr['data'], $user_item);
       // array_push($user_arr['test'], $users);
        }
        
        echo json_encode($user_arr);
        // echo json_encode($user_arr);
    } else {
        echo json_encode(array('message' => 'No users avaialble'));
    }
?>