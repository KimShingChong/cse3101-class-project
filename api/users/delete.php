<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: Delete');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/Database.php';
    include_once '../../models/user.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog user object
    $user = new User($db);

    // Get $_user Data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to be deleted
    $user->id = $data->id;

    // Update the user
    if($user->delete()){
        echo json_encode(array("message" => "User Deleted"));
    } else {
        echo json_encode(array("message" => "User Not Deleted"));

    }
?>