<?php 
    class Task_Comment {
        // Params
        private $conn;
        private $table = 'task_comments';

        //properties
        public $id;
        public $author;
        public $body;
        public $task_id;
        public $created_at;

        public function __construct($db){
            $this->conn = $db;
        }

        public function create(){
            // Query
            $query = 'INSERT INTO '.$this->table.'
                      SET
                        author = :author,
                        body = :body,
                        task_id = :task_id';
            
            // Prepare Statement
            $stmt = $this->conn->prepare($query);
            // Saniitze Data
            $this->author = htmlspecialchars(strip_tags($this->author)); 
            $this->body = htmlspecialchars(strip_tags($this->body)); 
            $this->task_id = htmlspecialchars(strip_tags($this->task_id));
            
            echo $this->task_id;
            // Bind Params
            $stmt->bindParam(':author', $this->author);
            $stmt->bindParam(':body', $this->body);
            $stmt->bindParam(':task_id', $this->task_id);

            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function read(){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as author, 
                        p.title as project,
                        t.id, t.body, t.created_at
                     FROM '.$this->table.' t
                     LEFT JOIN
                        users u ON t.author = u.id
                     LEFT JOIN
                        projects p on t.task_id = p.id
                     ORDER BY
                        p.created_at DESC';
            
            // prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute Statement
            $stmt->execute();

            return $stmt;
        }
        
        public function read_single($id){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as author, 
                        t.title as task,
                        tc.id, tc.body, tc.created_at
                     FROM '.$this->table.' tc
                     LEFT JOIN
                        users u ON tc.author = u.id
                     LEFT JOIN
                        tasks t on tc.task_id = t.id
                     WHERE
                        tc.task_id = :id
                     ORDER BY
                        tc.created_at DESC';
            
            // prepare statement
            $stmt = $this->conn->prepare($query);

            // Bind Statement
            $stmt->bindParam(':id', $id);

            // Execute Statement
            $stmt->execute();

            if($stmt->rowCount() > 0){
                $comments = array();

                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $comment_data = array(
                        'id' => $id,
                        'author' => $author,
                        'body' => $body,
                        'created_at' => $created_at
                    );
                    array_push($comments, $comment_data);
                }
                return $comments;
            }
            
           return array( "message" => 'No Comments Available');
        
        }

        public function update(){
            // Query
            $query = 'UPDATE '.$this->table.' 
                      SET
                        body = :body
                      WHERE
                        id = :id';
            
            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Saniitze Data
            $this->id = htmlspecialchars(strip_tags($this->id)); 
            $this->body = htmlspecialchars(strip_tags($this->body)); 

            // Bind Params
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':body', $this->body);

            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function delete(){
            // Query
            $query = 'DELETE FROM '.$this->table. ' WHERE id = :id';

            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // bind param
            $stmt->bindParam(':id', $this->id);

            // Execute
            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;

        }
    }
?>