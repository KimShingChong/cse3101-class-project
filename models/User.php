<?php 
    class User{
        // Params
        private $conn;
        private $table = 'users';

        // Properties
        public $id;
        public $first_name;
        public $last_name;
        public $email;
        public $password;
        public $account_type;
        public $created_at;

        // Constructor
        public function __construct($db){
            $this->conn = $db;
        }

        public function login(){
            // Query
            $query = 'SELECT id, first_name, last_name, email
                      FROM '. $this->table .'
                      WHERE 
                        email = :email AND
                        password = :password
                      LIMIT
                        0,1';
            
            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Sanitize Data
            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->password = htmlspecialchars(strip_tags($this->password));

            // Bind Params
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);

            // Execute Statement
            if($stmt->execute()){
                return $stmt;
            } 

            printf("Error: %s.\n", $stmt->error);
            return false;

        }

        public function get_all(){
          //Query
            $query = 'SELECT a.account_type as account_type, u.id, u.first_name, u.last_name, u.email
                                FROM ' . $this->table . ' u
                                LEFT JOIN
                                  account_types a ON u.account_type = a.id
                                ORDER BY
                                  u.created_at DESC';
            
            //prepare statement
            $stmt = $this->conn->prepare($query);

            //execute statement
            $stmt->execute();

            return $stmt;
        }

        public function create(){
            // Create Query
            $query = 'INSERT INTO '. $this->table . '
            SET 
              email = :email,
              password = :password,
              first_name = :first_name,
              last_name = :last_name,
              account_type = :account_type';
        
            //prepare statement
            $stmt = $this->conn->prepare($query);
            
            // Sanitize Data
            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->password = htmlspecialchars(strip_tags($this->password));
            $this->first_name = htmlspecialchars(strip_tags($this->first_name));
            $this->last_name = htmlspecialchars(strip_tags($this->last_name));
            $this->account_type = htmlspecialchars(strip_tags($this->account_type));
    
            // Bind params
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);
            $stmt->bindParam(':first_name', $this->first_name);
            $stmt->bindParam(':last_name', $this->last_name);
            $stmt->bindParam(':account_type', $this->account_type);
    
            // Execute
            if($stmt->execute()){
              return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
    }

        public function update(){
          // Create Query
            $query = 'UPDATE '. $this->table . '
            SET 
              email = :email,
              password = :password,
              first_name = :first_name,
              last_name = :last_name,
              account_type = :account_type
            WHERE
              id = :id';
            
              // prepare statement
              $stmt = $this->conn->prepare($query);

              // Sanitize Data
            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->password = htmlspecialchars(strip_tags($this->password));
            $this->first_name = htmlspecialchars(strip_tags($this->first_name));
            $this->last_name = htmlspecialchars(strip_tags($this->last_name));
            $this->account_type = htmlspecialchars(strip_tags($this->account_type));
    
            // Bind params
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);
            $stmt->bindParam(':first_name', $this->first_name);
            $stmt->bindParam(':last_name', $this->last_name);
            $stmt->bindParam(':account_type', $this->account_type);
    
            // Execute
            if($stmt->execute()){
              return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function delete(){
          // Query
          $query = 'DELETE FROM '.$this->table.' 
                    WHERE
                      id = :id';
          
          // Prepare Statement
          $stmt = $this->conn->prepare($query);

          // Bind Params
          $stmt->bindParam(':id', $this->id);

          // Execute Query
          if($stmt->execute()){
            return true;
          }

          printf("Error: %s.\n", $stmt->error);
          return false;
        }
}
?>