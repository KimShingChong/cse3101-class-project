<?php 
    class Task {
        //  Params
        private $conn;
        private $table = 'tasks';

        // Properties
        public $id;
        public $title;
        public $project;
        public $assigned_to;
        public $status;
        public $start_date;
        public $end_date;
        public $created_by;

        public function __construct($db){
            $this->conn = $db;
        }

        public function create(){
            // Query
            $query = 'INSERT INTO '.$this->table.'
                     SET
                        title = :title,
                        project = :project,
                        assigned_to = :assigned_to,
                        start_date = :start_date,
                        end_date = :end_date,
                        created_by = :created_by';
            
            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Sanitize data
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->project = htmlspecialchars(strip_tags($this->project));
            $this->assigned_to = htmlspecialchars(strip_tags($this->assigned_to));
            $this->start_date = htmlspecialchars(strip_tags($this->start_date));
            $this->end_date = htmlspecialchars(strip_tags($this->end_date));
            $this->created_by = htmlspecialchars(strip_tags($this->created_by));

            // Bind params
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':project', $this->project);
            $stmt->bindParam(':assigned_to', $this->assigned_to);
            $stmt->bindParam(':start_date', $this->start_date);
            $stmt->bindParam(':end_date', $this->end_date);
            $stmt->bindParam(':created_by', $this->created_by);

            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function read(){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as created_by, 
                        concat(u.first_name, \' \', u.last_name) as assigned_to,
                        p.title as project,
                        t.id, t.title, t.start_date, t.end_date, t.created_at
                     FROM '.$this->table.' t
                     LEFT JOIN
                        users u ON t.created_by = u.id AND t.assigned_to
                     LEFT JOIN
                        projects p on t.project = p.id
                     ORDER BY
                        p.created_at DESC';
            
            // prepare statement
            $stmt = $this->conn->prepare($query);

            $stmt->execute();

            return $stmt;
        }

        public function read_single(){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as created_by, 
                        concat(u.first_name, \' \', u.last_name) as assigned_to,
                        p.title as project,
                        t.id, t.title, t.start_date, t.end_date, t.created_at
                     FROM '.$this->table.' t
                     LEFT JOIN
                        users u ON t.created_by = u.id AND t.assigned_to
                     LEFT JOIN
                        projects p on t.project = p.id
                     WHERE
                        t.id = :id
                     LIMIT
                        0,1';
            
            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Bind Params
            $stmt->bindParam(':id', $this->id);

            // Execute
            $stmt->execute();

            // Fetch Single
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // Define properties
            $this->id = $row['id'];
            $this->title = $row['title'];
            $this->project = $row['project'];
            $this->assigned_to = $row['assigned_to'];
            $this->start_date = $row['start_date'];
            $this->end_date = $row['end_date'];
            $this->created_by = $row['created_by'];
            $this->created_at = $row['created_at'];
        }

        public function update(){
            // Query
            $query = 'UPDATE '.$this->table.'
                     SET
                        title = :title,
                        project = :project,
                        assigned_to = :assigned_to,
                        start_date = :start_date,
                        end_date = :end_date,
                        created_by = :created_by
                    WHERE
                        id = :id';
            
            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Sanitize data
            $this->id = htmlspecialchars(strip_tags($this->id));
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->project = htmlspecialchars(strip_tags($this->project));
            $this->assigned_to = htmlspecialchars(strip_tags($this->assigned_to));
            $this->start_date = htmlspecialchars(strip_tags($this->start_date));
            $this->end_date = htmlspecialchars(strip_tags($this->end_date));
            $this->created_by = htmlspecialchars(strip_tags($this->created_by));

            // Bind params
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':project', $this->project);
            $stmt->bindParam(':assigned_to', $this->assigned_to);
            $stmt->bindParam(':start_date', $this->start_date);
            $stmt->bindParam(':end_date', $this->end_date);
            $stmt->bindParam(':created_by', $this->created_by);

            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function delete(){
            // Query
            $query = 'DELETE FROM '.$this->table.' WHERE id = :id';

            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Bind params
            $stmt->bindParam(':id', $this->id);

            // Execute
            if($stmt->execute()){
                return true;
            }
            printf("Error: %s.\n", $stmt->error);
            return false;

        }
    }
?>