<?php 

    class Project{

        // Params
        private $conn;
        private $table = 'projects';
        
        // Properties
        public $id;
        public $title;
        public $start_date;
        public $due_date;
        public $created_by;

        public function __construct($db){
            $this->conn = $db;
        }

        public function create(){
            // Query
            $query = 'INSERT INTO '.$this->table. '
            SET
                title = :title,
                start_date = :start_date,
                due_date = :due_date,
                created_by = :created_by';

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Sanitize data 
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->start_date = htmlspecialchars(strip_tags($this->start_date));
            $this->due_date = htmlspecialchars(strip_tags($this->due_date));
            $this->created_by = htmlspecialchars(strip_tags($this->created_by));

            // Bind params
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':start_date', $this->start_date);
            $stmt->bindParam(':due_date', $this->due_date);
            $stmt->bindParam(':created_by', $this->created_by);

            // Execute query
            if($stmt->execute()){
                return true;
            } 

            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function read(){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as created_by, p.id, p.title, 
                        p.start_date, p.due_date, p.created_at
                     FROM '.$this->table.' p
                     LEFT JOIN
                        users u ON p.created_by = u.id
                     ORDER BY
                        p.created_at DESC';

            // Prepare Query
            $stmt = $this->conn->prepare($query);

            // 
            $stmt->execute();

            return $stmt;
        }

        public function read_single(){
            // Query
            $query = 'SELECT 
                        concat(u.first_name, \' \', u.last_name) as created_by, p.id, p.title, 
                        p.start_date, p.due_date, p.created_at
                     FROM '.$this->table.' p
                     LEFT JOIN
                         users u ON p.created_by = u.id
                     WHERE
                        p.id = :id
                     LIMIT
                        0,1';

            // Prepare Query
            $stmt = $this->conn->prepare($query);

            //Bind Params
            $stmt->bindParam(':id', $this->id);

            // Execute statement
            $stmt->execute();

            // Fetch Array
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // Set Properties
            $this->id = $row['id'];
            $this->title = $row['title'];
            $this->start_date = $row['start_date'];
            $this->due_date = $row['due_date'];
            $this->created_by = $row['created_by'];
            $this->created_at = $row['created_at'];
        }

         public function update(){
            // Query
            $query = 'UPDATE '.$this->table.'
                      SET
                        title = :title,
                        start_date = :start_date,
                        due_date = :due_date
                      WHERE
                        id = :id';

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Bind Params
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':start_date', $this->start_date);
            $stmt->bindParam(':due_date', $this->due_date);

            // Execute Statement
            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;

        }

        public function delete(){
            // Query
            $query = 'DELETE FROM '.$this->table.'
                      WHERE
                        id = :id';
            
            // Prepare
            $stmt = $this->conn->prepare($query);

            // Bind params
            $stmt->bindParam(':id', $this->id);

            // Execute
            if($stmt->execute()){
                return true;
            }

            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }
?>