<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: Delete');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/DAtabase.php';
    include_once '../../models/Post.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog post object
    $post = new Post($db);

    // Get $_Post Data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to be deleted
    $post->id = $data->id;

    // Update the post
    if($post->delete()){
        echo json_encode(array("message" => "Post Deleted"));
    } else {
        echo json_encode(array("message" => "Post Not Deleted"));

    }
?>