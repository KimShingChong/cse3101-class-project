<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
   
    include_once '../../config/DAtabase.php';
    include_once '../../models/Post.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog post object
    $post = new Post($db);

    // Get $_Post Data
    $data = json_decode(file_get_contents("php://input"));

    // Set ID to be updated
    $post->id = $data->id;

    // Assign $data to Post
    $post->title = $data->title;
    $post->body = $data->body;
    $post->author = $data->author;
    $post->category_id = $data->category_id;

    // Update the post
    if($post->update()){
        echo json_encode(array("message" => "Post Updated"));
    } else {
        echo json_encode(array("message" => "Post Not Updated"));

    }
?>