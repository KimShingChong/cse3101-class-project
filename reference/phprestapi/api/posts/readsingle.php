<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/Database.php';
    include_once '../../models/Project.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog project object
    $project = new Project($db);

    // Get ID
    $project->id = isset($_GET['id']) ? $_GET['id'] : die();

    // Get project
    $project->read_single();

    // Create Array
    $project_arr = array(
        'id' => $project->id,
        'title' => $project->title,
        'body' => $project->body,
        'author' => $project->author,
        'category_id' => $project->category_id,
        'category_name' => $project->category_name,
    );

    // Display project
    print_r(json_encode($project_arr));
?>