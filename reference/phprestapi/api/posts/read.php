<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../../config/DAtabase.php';
    include_once '../../models/Post.php';

    //Instantiate DB
    $database = new Database();
    $db = $database->connect();

    //Instantiate blog post object
    $post = new Post($db);

    //Blogpost query
    $result = $post->read();

    //get row count
    $count = $result->rowCount();

    // Check if posts exist
    if($count > 0){
        // Create array if exist
        $post_arr = array();
        $post_arr['data'] = array();
        $post_arr['test'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $posts = array('message' => "meh");
            $post_item = array(
                'id' => $id,
                'title' => $title,
                'body' => html_entity_decode($body),
                'author' => $author,
                'category_id' => $category_id,
                'category_name' => $category_name
            );

        array_push($post_arr['data'], $post_item);
        array_push($post_arr['test'], $posts);
        }
        
        echo json_encode($post_arr);
        // echo json_encode($post_arr);
    } else {
        echo json_encode(array('message' => 'No posts avaialble'));
    }
?>