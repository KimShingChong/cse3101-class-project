<?php 
    $host = 'localhost';
    $user = 'root';
    $password = '';
    $dbname = 'posts';

    // set DSN (Data Source Name)
    $dsn = 'mysql:host='. $host .';dbname='. $dbname;

    // create a PDO instance
    $pdo = new PDO($dsn, $user, $password);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    # pdo query

    $stmt = $pdo->query('SELECT * FROM posts');

    // while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    //     echo $row['title'] . '<br>';
    // }

    // while($row = $stmt->fetch()){
    //     echo $row->title . '<br>';
    // }


    # Prepared statement (prepare and execute)

    // UNSAFE
    // $sql = "SELECT * FROM posts WHERE author = '$author'";

    // Fetch posts with prepared statements

    // // User inputs
    // $author = 'kim';
    // $isPublished = true;

    // //Positional Params
    // $sql = 'SELECT * FROM posts WHERE author = :author && WHERE isPublished = :isPublished';
    // $stmt = $pdo->prepare($sql);
    // $stmt->execute(['author' => $author, 'isPublished' => $isPublished]);
    // $posts = $stmt->fetchAll();

    // // var_dump($posts);

    // foreach($posts as $post){
    //     echo $post->title. '<br>';
    // }
    
    // $id = 1;
    // // FETCH SINGLE POST
    // $sql = 'SELECT * FROM posts WHERE id = :id';
    // $stmt = $pdo->prepare($sql);
    // $stmt->execute(['id' => $id]);
    // $posts = $stmt->fetch();

    // echo $posts -> body;

    // $author = 'kim';
    // // Row Count
    // $stmt = $pdo->prepare('SELECT * FROM posts WHERE author = ?');
    // $stmt->execute([$author]);
    // $postCount = $stmt->rowCount();

    // echo $postCount;


    // Insert
    // $id = 3;
    // $title = 'PDO Post';
    // $body = 'Created via php';
    // $author = 'PDO';

    // $sql = 'INSERT INTO posts(id, title, body, author) VALUES(:id, :title, :body, :author)';
    // $stmt = $pdo->prepare($sql);
    // $stmt->execute([':id' => $id, ':title' => $title, ':body' => $body, ':author' => $author]);
    // echo 'Check mysql';

    // Update
    // $id = 3;
    // $body = 'Updated via php :)';

    // $sql = 'UPDATE posts SET body = :body where id = :id';
    // $stmt = $pdo->prepare($sql);
    // $stmt->execute([':body' => $body, ':id' => $id]);
    // echo 'Check mysql';

    //Delete
    // $id = 1;

    // $sql = 'DELETE FROM posts WHERE id = :id';
    // $stmt = $pdo->prepare($sql);
    // $stmt->execute([':id' => $id]);
    // echo 'RIP';

    // Search
    $search = '%via%';
    $sql = 'SELECT * FROM posts WHERE body LIKE ?';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$search]);
    $posts = $stmt->fetchAll();
    // var_dump($posts);
    forEach($posts as $post){
        echo $post->body. '<br>';
    }








?>